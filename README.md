Spring Cloud Eureka Server - Service Registration and Discovery

This is a Spring Cloud Netflix Eureka Server.

This will allow client-side service discovery which will allow clients to find and communicate with each other. The only ‘fixed point’ in such an architecture consists of a service registry with which each service has to register.

A drawback is that all clients must implement a certain logic to interact with this fixed point. This assumes an additional network round trip before the actual request.

With Netflix Eureka each client can simultaneously act as a server, to replicate its status to a connected peer. In other words, a client retrieves a list of all connected peers of a service registry and makes all further requests to any other services through a load-balancing algorithm.

To be informed about the presence of a client, they have to send a heartbeat signal to the registry.

3 Microservices needs to be implemented.

1. a service registry (Eureka Server),
2. a REST service which registers itself at the registry (Eureka Client) and
3. a web-application, which is consuming the REST service as a registry-aware client (Spring Cloud Netflix Feign Client). 
   and also uses a @LoadBalanced Spring RestTemplate to be able to route REST requests to the correct Eureka Client registered with the Eureka Server. 

Self Preservation

eureka:
  server:
    enableSelfPreservation: false

Flag is to preserve the instances even when they do not send heartbeats.
Self-preservation is a feature where Eureka servers stop expiring instances from the registry when they do not receive heartbeats (from peers and client microservices) beyond a certain threshold.
    
Link to Netflix Eureka documentation - http://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/1.2.4.RELEASE/

Without the below property, starting the instance will by default wait for the default of 5 mins
waitTimeInMsWhenSyncEmpty

Eureka Client Settings.

Eureka clients will need to use the below properties.

Tells Eureka to register the service using the ip address instead of the container id. - Required for docker containers, else the clients will register using the docker container names, which will not work.
http://projects.spring.io/spring-cloud/spring-cloud.html#_prefer_ip_address

eureka:
  instance:
    preferIpAddress: true

Renews registration every 1 second. And if not received in 2 seconds, lease is expired and he's out.
    leaseRenewalIntervalInSeconds: 1
    leaseExpirationDurationInSeconds: 2

Actuator & Metrics.

Spring Boot Actuator provides a number of endpoints including the below.

/health     UP DOWN
/info       
/metrics

If actuator is set up for the Eureka clients, Eureka services will use the health info and puts it up as STATUS.


Docker commands to build & run the Eureka service.

`
docker build --build-arg JAR_FILE="webservice-registry-1.0-SNAPSHOT.jar" -t eureka-service:1.1 .

docker run -d -p 8761:8761 -e CONFIG_SERVICE_URL=http://192.168.99.100:8888 --name eureka-service eureka-service:1.1 -t
`

Optional Config service property mentioned, just in case the application.yaml is picked up from config server. In this case bootstrap.yml is required.

`
spring:
  application:
    name: webservice-registry
  cloud:
    config:
      uri: ${CONFIG_SERVICE_URL}
`

Check the below URL's once the service has been started up.

http://192.168.99.100:8761
http://192.168.99.100:8761/eureka/apps
